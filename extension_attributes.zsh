#!/bin/zsh

# Variables

# Any accounts that you do not want to be checked
# Change to empty if you do not want to skip any 
skip_users=("example1" "example2")


##########################################
# Dont edit below
##########################################


# Date 30 days ago in a comparable format
compare_date=$(date -v-30d +"%s")
echo "Compare date: $compare_date"  # Debug line

# Empty string to add to
stale_users=""

# Reading each user one by one and processing them
while IFS= read -r user; do
    echo "Checking user: $user"  # Debug line
    
    # Check if the current user is in the skip_users array, if not empty
    if [[ ${#skip_users[@]} -ne 0 && " ${skip_users[*]} " == *" $user "* ]]; then
        echo "Skipping User: $user"
        continue
    fi
    
    # Get the last login info of the user
    login_info=$(last $user | head -n 1)
    echo "Login info: $login_info"  # Debug line
    
    # Extracting complete date information
    login_month_day=$(echo $login_info | awk '{print $4 " " $5}')
    login_year=$(date +"%Y")
    
    # Adjust the year if the login month is December and the current month is January
    current_month=$(date +"%m")
    login_month=$(date -jf "%b" "$login_month_day" +"%m" 2>/dev/null)
    if [[ $login_month -eq 12 && $current_month -eq 1 ]]; then
        login_year=$((login_year - 1))
    fi
    
    login_date=$(date -jf "%b %d %Y" "$login_month_day $login_year" +"%s" 2>/dev/null)
    echo "Login date (seconds): $login_date"  # Debug line
    
    # Handling the case where the date conversion might fail
    if [[ -z "$login_date" ]]; then
        stale_users+="$user, "
        continue
    fi
    
    if (( login_date < compare_date )); then
        # If the login date is older than the compare date, consider the user as stale
        stale_users+="$user, "
    fi
done < <(dscl . list /Users UniqueID | awk '$2 >= 500 {print $1}')

# Remove the trailing comma and space
stale_users=${stale_users%, }

echo "<result>$stale_users</result>"
