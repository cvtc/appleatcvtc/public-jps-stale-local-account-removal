JPS Stale Local Account Removal
=======================================

Removes the stale accounts that are found from the "Local Accounts - Stale" script EA from a specific smart computer group using the "DeleteUser" MDM command. Can exclude certain accounts in the EA script.

## Reasoning for script
We had a number of stale accounts on our lab machines that were filling the storage. This along with the corresponding EA remedies that.

## Requirements
This script relies on a mobile device extension attribute being created with the following settings:

Display Name:
```
Local Accounts - Stale
```
Data Type:
```
String
```
Input Type:
```
Script
```

Edit the skip_accounts to include any accounts you want skipped, leave blank otherwise.
Copy and paste the contents of extension_attributes.zsh into the script text box

## Usage

### Local
Export the variables listed below into your environment:
* All variable values are described below.
* The first command will make your bash history ignore commands with whitespace in front of them. This will make it so your JPS_PASSWORD isn't in 
your terminal history.
```console
setopt HIST_IGNORE_SPACE
export JPS_URL=https://example.jamf.com
export JPS_USERNAME=exampleUsername
 export JPS_PASSWORD=examplePassword
export COMPUTER_GROUP_ID=1001
export USER_EA_ID=1001
```

Run these commands to install all dependencies and run the script:
```console
pip install pipenv
pipenv install --deploy
pipenv run python ./jps_stale_local_account_removal.py
```

### GitLab
Uses base Azure Function setup, consult [general documentation.](https://gitlab.com/cvtc/appleatcvtc/project-templates/-/tree/main/Python%20Azure%20Functions?ref_type=heads)

## Variables

### JPS_URL
* URL of your JPS Server
> https://example.jamfcloud.com

### JPS_USERNAME / JPS_PASSWORD
JPS account credentials with the following permissions set:
* Jamf Pro Server Objects
  * Computers
    * Read
    * Update
  * Smart Computer Group 
    * Read
* Jamf Pro Server Actions
    * Send Computer Delete User Account Command

### COMPUTER_GROUP_ID
ID of the computer group that we are removing stale accounts from.

Example: The computer group ID of the following group would be 419
> https://example.jamfcloud.com/smartComputerGroups.html?id=419&o=r

### USER_EA_ID
ID of the Extension Attribute that you created earlier

Example: The extension attribute ID of the following extension attribute would be 101
> https://example.jamfcloud.com/computerExtensionAttributes.html?id=101&o=r


## Authors
Bryan Weber (bweber26@cvtc.edu)

## Copyright
Copyright 2023 Chippewa Valley Technical College. It is provided under the MIT License. For more information, please see the LICENSE file in this repository.