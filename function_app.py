import azure.functions as func

import jps_stale_local_account_removal

app = func.FunctionApp()


@app.schedule(
    schedule="0 0 6 * * *", arg_name="myTimer", run_on_startup=False, use_monitor=False
)
def Daily(myTimer: func.TimerRequest) -> None:
    jps_stale_local_account_removal.main()
