import logging
from os import environ
from typing import Union
from jps_api_wrapper.classic import Classic

# Set up logging with a specific format that includes timestamp and log level
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def main():
    JPS_URL = environ["JPS_URL"]
    JPS_USERNAME = environ["JPS_USERNAME"]
    JPS_PASSWORD = environ["JPS_PASSWORD"]
    COMPUTER_GROUP_ID = environ["COMPUTER_GROUP_ID"]
    USER_EA_ID = int(environ["USER_EA_ID"])

    def delete_user_data(username: str, computer_id: Union[int, str]):
        """
        Create XML for user deletion request

        :param username: Username to delete
        :param computer_id: ID of the computer the user is being deleted from

        :returns: XML data for the computer command to delete the user
        """

        return f"""
        <computer_command>
            <general>
                <command>DeleteUser</command>
                <user_name>{username}</user_name>
            </general>
            <computers>
                <computer>
                    <id>{computer_id}</id>
                </computer>
            </computers>
        </computer_command>
        """

    with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic:
        # Retrieve a list of computer IDs from a specified computer group.
        computer_ids = [
            computer["id"]
            for computer in classic.get_computer_group(COMPUTER_GROUP_ID)[
                "computer_group"
            ]["computers"]
        ]

        # Iterate over each computer ID to process stale accounts.
        for computer_id in computer_ids:
            # Retrieve extension attributes for each computer.
            computer_eas = classic.get_computer(computer_id)["computer"][
                "extension_attributes"
            ]
            # Find the specific extension attribute by ID and get its value
            # (stale accounts list).
            stale_accounts = [
                ea["value"] for ea in computer_eas if ea["id"] == USER_EA_ID
            ][0]
            # Split the stale accounts string into a list and remove any
            # leading/trailing whitespace.
            stale_accounts = [account.strip() for account in stale_accounts.split(",")]

            # Iterate over the list of stale accounts.
            for stale_account in stale_accounts:
                # Skip any account that is in the list of excluded accounts or
                # if it's an empty string.
                if not stale_account:
                    continue
                # Use the API to create a command to delete the user from the
                # computer.
                classic.create_computer_command(
                    "DeleteUser", data=delete_user_data(stale_account, computer_id)
                )
                # Log the deletion of the account.
                logging.info(
                    f"Computer {computer_id}: Account {stale_account} deleted."
                )


if __name__ == "__main__":
    main()
